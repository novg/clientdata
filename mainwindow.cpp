#include "mainwindow.h"
#include "settingsdialog.h"

#include <QSqlDatabase>
#include <QSqlError>
#include <QLabel>
#include <QTableView>
#include <QDateTimeEdit>
#include <QLineEdit>
#include <QValidator>
#include <QBoxLayout>
#include <QAction>
#include <QToolBar>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QMessageBox>
#include <QSettings>
#include <QFileDialog>
#include <QHeaderView>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    readSettings();
    createWidgets();
    createActions();
    createConnection();

    connect(firstDateTime, SIGNAL(editingFinished()), SLOT(slotShowTables()));
    connect(lastDateTime, SIGNAL(editingFinished()), SLOT(slotShowTables()));
    connect(inNumber, SIGNAL(returnPressed()), SLOT(slotShowTables()));
    connect(outNumber, SIGNAL(returnPressed()), SLOT(slotShowTables()));
    connect(actionAbout, SIGNAL(triggered()), SLOT(slotAbout()));
    connect(actionSettings, SIGNAL(triggered()), SLOT(slotSettings()));
    connect(actionSave, SIGNAL(triggered()), SLOT(slotSaveToFile()));
}

MainWindow::~MainWindow()
{

}

void MainWindow::slotShowTables()
{
    if (tableName.isEmpty())
        return;

    qmodel = new QSqlQueryModel(this);
    QString strDirIn  = inNumber->text();
    QString strDirOut = outNumber->text();
    QString strFirstDateTime =
            firstDateTime->dateTime().toString("yyyy-MM-dd hh:mm");
    QString strLastDateTime =
            lastDateTime->dateTime().toString("yyyy-MM-dd hh:mm");
    QString strQuery;
    QString strRowsCount;

    if (!strDirIn.isEmpty())
        strDirIn = addQuotes(strDirIn);
    if (!strDirOut.isEmpty())
        strDirOut = addQuotes(strDirOut);

    if (strDirIn.isEmpty() and strDirOut.isEmpty()) {
        strQuery = QString("SELECT datetime, duration, numin, numout FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3';"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                ;
        strRowsCount = QString("SELECT COUNT(*) FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3';"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                ;
    } else if (!strDirIn.isEmpty() and strDirOut.isEmpty()) {
        strQuery = QString("SELECT datetime, duration, numin, numout FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3' "
                           "AND numin IN (%4);"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                .arg(strDirIn)
                ;
        strRowsCount = QString("SELECT COUNT(*) FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3' "
                           "AND numin IN (%4);"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                .arg(strDirIn)
                ;
    } else if (strDirIn.isEmpty() and !strDirOut.isEmpty()) {
        strQuery = QString("SELECT datetime, duration, numin, numout FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3' "
                           "AND numout IN (%4);"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                .arg(strDirOut)
                ;
        strRowsCount = QString("SELECT COUNT(*) FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3' "
                           "AND numout IN (%4);"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                .arg(strDirOut)
                ;
    } else {
        strQuery = QString("SELECT datetime, duration, numin, numout FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3' "
                           "AND numin IN (%4) AND numout IN (%5);"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                .arg(strDirIn)
                .arg(strDirOut)
                ;
        strRowsCount = QString("SELECT COUNT(*) FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3' "
                           "AND numin IN (%4) AND numout IN (%5);"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                .arg(strDirIn)
                .arg(strDirOut)
                ;

    }

    showRowsCount(strRowsCount);
    qmodel->setQuery(strQuery);

    if (qmodel->lastError().isValid())
        qDebug() << qmodel->lastError().text();

    qmodel->setHeaderData(0, Qt::Horizontal, "Дата время");
    qmodel->setHeaderData(1, Qt::Horizontal, "Длительность вызова");
    qmodel->setHeaderData(2, Qt::Horizontal, "Набираемый номер");
    qmodel->setHeaderData(3, Qt::Horizontal, "Вызывающий номер");
    tableView->setModel(qmodel);
    tableView->resizeColumnsToContents();
    tableView->resizeRowsToContents();
}

void MainWindow::slotAbout()
{
    QMessageBox::about(0, "About", "<h1><b><i><u>clientData ver. 0.8</u></i></b></h1>"
                       "produced by: A. Novgorodskiy<br/>"
                       "email: <a href=\"mailto:novg.novg@gmail.com\">novg.novg@gmail.com</a><br/>"
                       );
}

void MainWindow::slotSettings()
{
    SettingsDialog *settingsDialog = new SettingsDialog;
    if (settingsDialog->exec() == QDialog::Accepted) {
        hostName = settingsDialog->host();
        baseName = settingsDialog->base();
        userName = settingsDialog->user();
        password = settingsDialog->password();
        writeSettings();
        createConnection();
    }

    settingsDialog->deleteLater();
}

void MainWindow::slotSaveToFile()
{
    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save to file"),
                                 "", tr("Text files (*.txt);;All files (*)"));
    if (saveFileName != "") {
        QFile saveFile(saveFileName);
        if (saveFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream stream(&saveFile);
            int length = 74;
            stream << printLine(length);
            stream << tr("Дата\t\tВремя") << "\t\t"
                   << tr("Длительность")  << "\t"
                   << tr("Набираемый")    << "\t"
                   << tr("Вызывающий")    << "\n"
                      ;
            stream << tr("вызова")
                   << tr("\t\tвызова")
                   << tr("\t\tвызова")
                   << tr("\t\tномер")
                   << tr("\t\tномер")
                   << "\n";
            stream << printLine(length);

            while (qmodel->canFetchMore())
                qmodel->fetchMore();

            int rowsCount   = tableView->verticalHeader()->count();
            int columnCount = tableView->horizontalHeader()->count();

            QString line;
            for (int row = 0; row < rowsCount; ++row) {
                for (int col = 0; col < columnCount; ++col) {
                    QModelIndex index = qmodel->index(row, col, QModelIndex());
                    line = qmodel->data(index).toString();
                    stream << line.replace("T", "\t")
                           << delimeter(col, line.length());
//                    stream << qmodel->data(index).toString() << delimeter(col);
                }
            }

            stream << printLine(length);
            saveFile.close();
        }
    }

}

QString MainWindow::printLine(int length)
{
    QString line;
    for (int i = 0; i < length; ++i)
        line += "-";

    line += "\n";
    return line;
}

QString MainWindow::delimeter(int column, int len)
{
    switch (column) {
    case 0:
        return "\t";
        break;
    case 1:
        return "\t";
        break;
    case 2:
        if (len > 5)
            return "\t";
        else
            return "\t\t";
        break;
    default:
        return "\n";
        break;
    }
}

void MainWindow::readSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("/connectSQL");
    hostName = settings.value("/host").toString();
    baseName = settings.value("/base").toString();
    userName = settings.value("/user").toString();
    password = settings.value("/password").toString();
    settings.endGroup();
}

void MainWindow::writeSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("/connectSQL");
    settings.setValue("/host", hostName);
    settings.setValue("/base", baseName);
    settings.setValue("/user", userName);
    settings.setValue("/password", password);
    settings.endGroup();
}

bool MainWindow::createConnection()
{
    QSqlDatabase db;
    if(QSqlDatabase::contains(QSqlDatabase::defaultConnection))
        db = QSqlDatabase::database();
    else
        db = QSqlDatabase::addDatabase("QPSQL");

//    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName(hostName);
//    db.setPort(5432);
    db.setDatabaseName(baseName);
    db.setUserName(userName);
    db.setPassword(password);

    if (!db.open()) {
        QMessageBox::critical(0, tr("Ошибка"), tr("Невозможно открыть базу данных"),
                              QMessageBox::Ok);
        return false;
    }

    tableName = db.tables(QSql::Tables)[0];

    return true;
}

void MainWindow::createWidgets()
{
    QWidget *widgetPanel = new QWidget;
    QLabel *labelFirstDate = new QLabel(tr("Начало периода"));
    QLabel  *labelLastDate   = new QLabel(tr("Конец периода:"));
    QLabel  *labelInNumber   = new QLabel(tr("Набираемый номер:"));
    QLabel  *labelOutNumber  = new QLabel(tr("Вызывающий номер:"));
    QWidget *widgetCentral   = new QWidget;
    labelRowsCount = new QLabel(tr("Всего соединений: "));

    tableView     = new QTableView;
//    firstDateTime = new QDateTimeEdit(QDateTime(QDate::currentDate(), QTime(0, 0)));
    firstDateTime = new QDateTimeEdit(QDateTime::currentDateTime().addSecs(-300));
    firstDateTime->setDisplayFormat("yyyy-MM-dd hh:mm");
    lastDateTime  = new QDateTimeEdit(QDateTime::currentDateTime());
    lastDateTime->setDisplayFormat("yyyy-MM-dd hh:mm");
    inNumber      = new QLineEdit;
    outNumber     = new QLineEdit;

    QValidator *validator = new QRegExpValidator(QRegExp("[0-9,]*"));
    inNumber->setValidator(validator);
    outNumber->setValidator(validator);

    // Layout setup
    QVBoxLayout *inNumberLayout  = new QVBoxLayout;
    inNumberLayout->addWidget(labelInNumber);
    inNumberLayout->addWidget(inNumber);

    QVBoxLayout *outNumberLayout = new QVBoxLayout;
    outNumberLayout->addWidget(labelOutNumber);
    outNumberLayout->addWidget(outNumber);

    QVBoxLayout *firstDateLayout = new QVBoxLayout;
    firstDateLayout->addWidget(labelFirstDate);
    firstDateLayout->addWidget(firstDateTime);

    QVBoxLayout *lastDateLayout  = new QVBoxLayout;
    lastDateLayout->addWidget(labelLastDate);
    lastDateLayout->addWidget(lastDateTime);

    QHBoxLayout *groupLayout = new QHBoxLayout;
    groupLayout->addLayout(firstDateLayout);
    groupLayout->addLayout(lastDateLayout);
    groupLayout->addStretch(1);
    groupLayout->addLayout(inNumberLayout);
    groupLayout->addLayout(outNumberLayout);
    widgetPanel->setLayout(groupLayout);

    QVBoxLayout *commonLayout    = new QVBoxLayout;
    commonLayout->addWidget(widgetPanel);
    commonLayout->addWidget(tableView);
    commonLayout->addWidget(labelRowsCount);
    widgetCentral->setLayout(commonLayout);

    setCentralWidget(widgetCentral);
}

void MainWindow::createActions()
{
    actionSettings = new QAction(tr("Настройки соединения"), this);
    actionSettings->setToolTip(tr("Настройки соединения"));
    actionSettings->setStatusTip(tr("Настройки соединения"));
    actionSettings->setWhatsThis(tr("Настройки соединения"));
    actionSettings->setIcon(QIcon(":/images/cog-icon-2-48x48.png"));

    actionSave = new QAction(tr("Сохранить в файл"), this);
    actionSave->setToolTip(tr("Сохранить в файл"));
    actionSave->setStatusTip(tr("Сохранить в файл"));
    actionSave->setWhatsThis(tr("Сохранить в файл"));
    actionSave->setIcon(QIcon(":/images/floppy.png"));

    actionAbout = new QAction(tr("О программе"), this);
    actionAbout->setToolTip(tr("О программе"));
    actionAbout->setStatusTip(tr("О программе"));
    actionAbout->setWhatsThis(tr("О программе"));
    actionAbout->setIcon(QIcon(":/images/about.png"));

    QList<QAction *> actions;
    actions << actionSettings << actionSave << actionAbout;

    QToolBar *toolBar;
    toolBar = addToolBar(tr("Панель настроек"));
    toolBar->addActions(actions);
}

void MainWindow::showRowsCount(const QString &strQuery)
{
    QSqlQuery sqlQuery(strQuery);
    sqlQuery.first();
    labelRowsCount->setText(tr("Всего соединений: %1")
                            .arg(sqlQuery.value(0).toInt()));
}

QString MainWindow::addQuotes(const QString &str)
{
    QStringList list = str.split(",");
    for (auto &iter : list)
        iter = "'" + iter + "'";

    return list.join(",");
}
