#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QLabel;
class QTableView;
class QDateTimeEdit;
class QLineEdit;
class QAction;
class QSqlQueryModel;
class QSqlDatabase;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void slotShowTables();
    void slotAbout();
    void slotSettings();
    void slotSaveToFile();
    QString printLine(int length);
    QString delimeter(int column, int len);

private:
    void readSettings();
    void writeSettings();
    bool createConnection();
    void createWidgets();
    void createActions();
    void showRowsCount(const QString &strQuery);
    QString addQuotes(const QString &str);


private:
    QLabel *labelRowsCount;
    QTableView *tableView;
    QDateTimeEdit *firstDateTime;
    QDateTimeEdit *lastDateTime;
    QLineEdit *inNumber;
    QLineEdit *outNumber;
    QAction *actionSettings;
    QAction *actionSave;
    QAction *actionAbout;
    QString hostName;
    QString baseName;
    QString userName;
    QString password;
    QString tableName;
    QSqlQueryModel *qmodel;
};

#endif // MAINWINDOW_H
