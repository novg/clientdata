#-------------------------------------------------
#
# Project created by QtCreator 2015-01-22T14:57:38
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = clientData
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    settingsdialog.cpp

HEADERS  += mainwindow.h \
    settingsdialog.h

CONFIG += c++14

RC_FILE = icon.rc

RESOURCES += \
    images.qrc

DISTFILES +=
